const express = require("express");
const winston = require("winston");
const expressWinston = require("express-winston");
const cors = require("cors");

const PORT = parseInt(process.env.PORT, 10) || 7000;

const userRouter = require("./routes/userRoutes");
const messageRouter = require("./routes/messageRoutes");
const miscController = require("./controllers/miscController");

const server = express();

const corsOptions = {
	"origin": "*",
	"methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
	"preflightContinue": false,
	"optionsSuccessStatus": 204
};

server.use(cors(corsOptions));

server.use(expressWinston.logger({
	transports: [
		new winston.transports.Console()
	],
	format: winston.format.simple(),
	meta: false,
	msg: "HTTP {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}",
	expressFormat: false,
	colorize: true
}));


server.use(express.json());
server.use(express.urlencoded({ extended: false }));

server.use("/users", userRouter);
server.use("/messages", messageRouter);
server.post("/login", miscController.login);
server.use("*", miscController.notFound);


server.use((err, req, res, next) => {
	const statusCode = err.statusCode || 500;
	res.status(statusCode).json({
		error: true,
		message: err.message
	});
});

server.listen(PORT, err => {
	if (err) throw err;
	console.log("Server is ready on port: " + PORT);
});