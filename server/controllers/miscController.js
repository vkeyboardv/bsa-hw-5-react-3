const fs = require("fs");

const filename = "./db/users.json";

exports.notFound = (req, res, next) => {
	const err = new Error("Specified endpoint was not found");
	err.status = 404;
	next(err);
};

exports.login = (req, res, next) => {
	let usersData = {};
	try {
		fs.readFile(filename, (err, data) => {
			if (err) throw err;
			usersData = JSON.parse(data);
			if (usersData.length === 0) {
				const err = new Error("No users found in DB");
				err.status = 400;
				return next(err);
			}
			const loginUser = req.body;
			const dbUser = usersData.find(user => user.name === loginUser.login);
			if (dbUser && dbUser.password === loginUser.password) {
				res.status(200).json({ ...dbUser });
			} else {
				res.status(400).json({
					error: true,
					message: "Authentication has failed"
				});
			}
		});
	} catch (err) {
		return res.status(err.status || 500).json({
			error: true,
			message: err.message
		});
	}
};