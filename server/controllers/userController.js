const fs = require("fs");
const utils = require("../utils/utils");

const filename = "./db/users.json";

exports.getAllUsers = function getAllUsers(req, res, next) {
	try {
		fs.readFile(filename, (err, data) => {
			if (err) throw err;
			data = JSON.parse(data);
			if (data.length === 0) {
				const err = new Error("No users found in DB");
				err.status = 400;
				return next(err);
			}
			return res.status(200).json(data);
		});
	} catch (err) {
		return res.status(err.status || 500).json({
			error: true,
			message: err.message
		});
	}
};

exports.getUser = function getUser(req, res, next) {
	try {
		const id = req.params.id;
		fs.readFile(filename, (err, data) => {
			if (err) throw err;
			data = JSON.parse(data);
			if (data.length === 0) {
				const err = new Error("No users found in DB");
				err.status = 400;
				return next(err);
			}
			const user = data.find(item => item.id == id);
			if (!user) {
				const err = new Error("No user found with specified id");
				err.status = 400;
				return next(err);
			}
			return res.status(200).json(user);
		});
	} catch (err) {
		return res.status(err.status || 500).json({
			error: true,
			message: err.message
		});
	}
};

exports.createUser = async function createUser(req, res, next) {
	try {
		fs.readFile(filename, (err, data) => {
			if (err) throw err;
			data = JSON.parse(data);
			let id = null;
			if (data.length !== 0) {
				id = utils.newID(data);
			} else {
				id = "1";
			}
			const newUser = { ...req.body, id };
			data.push(newUser);
			utils.writeToFile(filename, data);
			return res.status(200).json({
				success: true,
				message: newUser
			});
		});
	} catch (err) {
		res.status(err.status || 500).json({
			error: true,
			message: err.message
		});
	}
};

exports.updateUser = async function updateUser(req, res, next) {
	try {
		const id = req.params.id;
		fs.readFile(filename, (err, data) => {
			if (err) throw err;
			data = JSON.parse(data);
			const userIndex = data.findIndex(item => item.id == id);
			if (!(userIndex + 1)) {
				const err = new Error("No users found with specified id");
				err.status = 400;
				return next(err);
			}
			const newUser = { ...data[userIndex], ...req.body };
			data[userIndex] = newUser;
			utils.writeToFile(filename, data);
			return res.status(200).json({
				success: true,
				message: newUser
			});
		});
	} catch (err) {
		res.status(err.status || 500).json({
			error: true,
			message: err.message
		});
	}
};


exports.deleteUser = async function deleteUser(req, res, next) {
	try {
		const id = req.params.id;
		fs.readFile(filename, (err, data) => {
			if (err) throw err;
			data = JSON.parse(data);
			const newData = data.filter(item => item.id !== id);
			utils.writeToFile(filename, newData);
			return res.status(200).json({
				success: true
			});
		});
	} catch (err) {
		res.status(err.status || 500).json({
			error: true,
			message: err.message
		});
	}
};