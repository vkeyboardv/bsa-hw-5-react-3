const fs = require("fs");
const utils = require("../utils/utils");

const filename = "./db/messages.json";

exports.getAllMessages = function getAllMessages(req, res, next) {
	try {
		fs.readFile(filename, (err, data) => {
			if (err) throw err;
			data = JSON.parse(data);
			if (data.length === 0) {
				const err = new Error("No messages found in DB");
				err.status = 400;
				return next(err);
			}
			return res.status(200).json(data);
		});
	} catch (err) {
		res.status(err.status || 500).json({
			error: true,
			message: err.message
		});
	}
};

exports.getMessage = function getMessage(req, res, next) {
	try {
		const id = req.params.id;
		fs.readFile(filename, (err, data) => {
			if (err) throw err;
			data = JSON.parse(data);
			if (data.length === 0) {
				const err = new Error("No messages found in DB");
				err.status = 400;
				return next(err);
			}
			const message = data.find(item => item.id == id);
			if (!message) {
				const err = new Error("No messages found with specified id");
				err.status = 400;
				return next(err);
			}
			return res.status(200).json(message);
		});
	} catch (err) {
		res.status(err.status || 500).json({
			error: true,
			message: err.message
		});
	}
};

exports.createMessage = async function createMessage(req, res, next) {
	try {
		fs.readFile(filename, (err, data) => {
			if (err) throw err;
			data = JSON.parse(data);
			let id = null;
			if (data.length !== 0) {
				id = utils.newID(data);
			} else {
				id = "1001";
			}
			const newMessage = { id, ...req.body };
			data.push(newMessage);
			utils.writeToFile(filename, data);
			return res.status(200).json({
				success: true,
				message: newMessage
			});
		});
	} catch (err) {
		res.status(err.status || 500).json({
			error: true,
			message: err.message
		});
	}
};

exports.updateMessage = async function updateMessage(req, res, next) {
	try {
		const id = req.params.id;
		fs.readFile(filename, (err, data) => {
			if (err) throw err;
			data = JSON.parse(data);
			const messageIndex = data.findIndex(item => item.id == id);
			if (!(messageIndex + 1)) {
				const err = new Error("No messages found with specified id");
				err.status = 400;
				return next(err);
			}
			const newMessage = { ...data[messageIndex], ...req.body };
			data[messageIndex] = newMessage;
			utils.writeToFile(filename, data);
			return res.status(200).json({
				success: true,
				message: newMessage
			});
		});
	} catch (err) {
		res.status(err.status || 500).json({
			error: true,
			message: err.message
		});
	}
};

exports.deleteMessage = async function deleteMessage(req, res, next) {
	try {
		const id = req.params.id;
		fs.readFile(filename, (err, data) => {
			if (err) throw err;
			data = JSON.parse(data);
			const newData = data.filter(item => item.id !== id);
			utils.writeToFile(filename, newData);
			return res.status(200).json({
				success: true
			});
		});
	} catch (err) {
		res.status(err.status || 500).json({
			error: true,
			message: err.message
		});
	}
};