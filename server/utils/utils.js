const fs = require("fs");

exports.newID = (arr) => {
	if (arr.length > 0) {
		const newId = +(arr[arr.length - 1].id) + 1;
		return newId.toString();
	}
	return "1";
};

exports.writeToFile = (filename, content) => {
	fs.writeFile(filename, JSON.stringify(content), "utf8", (err) => {
		if (err) {
			console.log(err);
		}
	});
};