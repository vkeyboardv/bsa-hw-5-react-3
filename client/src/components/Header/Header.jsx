import React from "react";
import Participants from "./Participants";
import MessagesCounter from "./MessagesCounter";
import LastMessageDate from "./LastMessageDate";

import lodash from "lodash";

class Header extends React.Component {
	render() {
		const { messages, isLoading } = this.props;
		const messagesLength = messages.length;
		const uniqueUsers = lodash.uniqBy(messages, "user").length;

		if (!isLoading) {
			return (
				<div className="main-chat-header">
					<div className="main-chat-name">
						<h2>Chat Room</h2>
					</div>
					<div className="main-chat-participants">
						<Participants uniqueUsers={uniqueUsers} />
					</div>
					<div className="main-chat-header-messages">
						<MessagesCounter messagesLength={messagesLength} />
					</div>
					<div className="main-chat-last-message-date">
						<LastMessageDate messages={this.props.messages} />
					</div>
				</div>
			);
		} else {
			return null;
		}
	}
}

export default Header;
