import React from "react";

class LastMessageDate extends React.Component {
	render() {
		const lastMessage = this.props.messages[this.props.messages.length - 1];
		if (lastMessage) {
			const lastMessageSplitedDate = lastMessage.created_at.split(/-|\s|:/);
			const newDate = new Date(
				lastMessageSplitedDate[0],
				lastMessageSplitedDate[1] - 1,
				lastMessageSplitedDate[2],
				lastMessageSplitedDate[3],
				lastMessageSplitedDate[4],
				lastMessageSplitedDate[5]);
			return (<div className="main-chat-last-message-date-info">
				Last message: <span>{checkDateDifference(newDate)}</span>
			</div>);
		}
		return null;
	}
}

function checkDateDifference(date) {
	const today = new Date();
	const yesterday = new Date();
	yesterday.setDate(today.getDate() - 1);
	if (date.toLocaleDateString() === today.toLocaleDateString()) {
		return "Today";
	} else if (date.toLocaleDateString() === yesterday.toLocaleDateString()) {
		//console.log(today.toLocaleDateString());
		return "Yesterday";
	}
	return date.toLocaleDateString("en-US", {
		day: "numeric",
		month: "long"
	});
}

export default LastMessageDate;
