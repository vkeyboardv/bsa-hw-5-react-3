import React, { Component } from "react";
import { Switch, Route, Redirect, Link } from "react-router-dom";
import LoginPage from "../../containers/Login/Login";
import User from "../../containers/User/User";
import Users from "../../containers/Users/Users";
import ChatPage from "../../containers/Chat/Chat";
import { connect } from "react-redux";
import logo from "../../logo.svg";
import SecureRoute from "../SecureRoute/SecureRoute";

class Main extends Component {
	render() {
		const { isLoggedIn, isAdmin, currentUser } = this.props.auth;
		return (
			<>
				{isLoggedIn ? (
					<>
						<div className="app-menu">
							<Link to='/chat'>Chat</Link>
							<Link to='/users'>Users</Link>
						</div>
						<div className='current-user'>
							Current user: {(currentUser && currentUser.name) ? currentUser.name : "Incognito"}
						</div>
					</>
				) : (
					<img src={logo} className="App-logo" alt="logo" />
				)
				}
				{isLoggedIn && isAdmin ? (<Redirect to='/users' />) : null}
				{isLoggedIn && !isAdmin ? (<Redirect to='/chat' />) : null}
				<Switch>
					<Route exact path='/' render={() => (isLoggedIn ? <Redirect to="/chat" /> : <Redirect to="/login" />)} />
					<Route path='/login' component={LoginPage} />
					<SecureRoute exact path='/user' component={User} access={isLoggedIn && isAdmin} />
					<SecureRoute path='/user/:id' component={User} access={isLoggedIn && isAdmin} />
					<SecureRoute exact path='/users' component={Users} access={isLoggedIn && isAdmin} />
					<SecureRoute exact path='/chat' component={ChatPage} access={isLoggedIn} />
					<Redirect to="/login" />
				</Switch>
			</>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		auth: state.auth
	};
};

export default connect(mapStateToProps)(Main);
