import React from "react";
import Message from "./Message";

class MessageList extends React.Component {
	getMessages() {
		return this.props.messages.map(
			message => <Message message={message} key={message.id} deleteMsg={this.props.deleteMsg}
													updateMsg={this.props.updateMsg} />);
	}

	render() {
		return (
			<div className="main-chat-messages">
				<div className="messages">
					{this.getMessages()}
				</div>
			</div>);
	}
}

export default MessageList;
