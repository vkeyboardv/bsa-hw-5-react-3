import React from "react";
import uuidv4 from "uuid/v4";
import moment from "moment";
import {
	InputGroup,
	InputGroupAddon,
	Input,
	Button
} from "reactstrap";
import { connect } from "react-redux";

class MessageInput extends React.Component {

	state = {
		msg: ""
	};

	handleAddMsg = () => {
		const { msg } = this.state;
		const currentUser = this.props.auth.currentUser || {};
		const message = msg.trim();

		if (message.length > 0) {
			const newMsg = {
				id: uuidv4(),
				user: currentUser.name || "Incognito",
				avatar: "https://bizraise.pro/wp-content/uploads/2014/09/no-avatar-300x300.png",
				created_at: `${moment().format("YYYY-MM-DD HH:mm:ss")}`,
				message: message,
				marked_read: false
			};
			this.props.addMsg(newMsg);
			this.setState({ msg: "" });
		}
	};

	render() {
		return (
			<div className='message-input-block'>
				<InputGroup size="sm">
					<Input type="text" onChange={ev => this.setState({ msg: ev.target.value })} value={this.state.msg} />
					<InputGroupAddon addonType="prepend"><Button type="submit"
																											 onClick={e => this.handleAddMsg()}>Send</Button></InputGroupAddon>
				</InputGroup>
			</div>
		);
	}
}


const mapStateToProps = state => {
	return {
		auth: state.auth
	};
};

export default connect(mapStateToProps, {})(MessageInput);
