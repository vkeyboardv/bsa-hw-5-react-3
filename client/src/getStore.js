import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./rootReducer";
import initializeSagas from "./sagas/initSaga";

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancers = composeEnhancers(
	applyMiddleware(sagaMiddleware)
);

export default createStore(rootReducer, {}, enhancers);

sagaMiddleware.run(initializeSagas);

