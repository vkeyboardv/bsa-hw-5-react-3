import { ADD_USER, UPDATE_USER, DELETE_USER, GET_USERS, GET_USERS_SUCCESS } from "./actionTypes";
import uuidv1 from "uuid/v1";

export const addUser = data => ({
	type: ADD_USER,
	payload: {
		id: uuidv1(),
		data
	}
});

export const updateUser = (id, data) => {
	return {
		type: UPDATE_USER,
		payload: { id, data }
	};
};

export const deleteUser = (id) => {
	return {
		type: DELETE_USER,
		payload: id
	};
};

export const getUsers = () => {
	return {
		type: GET_USERS
	};
};

export const getUsersSuccess = (data) => {
	return {
		type: GET_USERS_SUCCESS,
		payload: data
	};
};