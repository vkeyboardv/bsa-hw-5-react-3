import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { addUser, updateUser, deleteUser, getUsers } from "./actionCreators";
import SpinnerElement from "../../components/SpinnerElement";

class UserList extends Component {

	componentDidMount() {
		this.props.getUsers();
	}

	handleEditButtonClick = (id) => {
		this.props.history.push(`/user/${id}`);
	};

	handleDeleteButtonClick = (id) => {
		this.props.deleteUser(id);
	};

	handleAddUserClick = () => {
		this.props.history.push("/user");
	};

	render() {
		const { users } = this.props;
		if (users.length === 0) {
			return (<div className="spinner mt-3">
				<SpinnerElement />
			</div>);
		}
		return (
			<div className="row">
				<div className="list-group col-10 mt-3">
					{
						users.map(user => {
							const { id, name, surname, email } = user;
							return (
								<div className="container list-group-item" key={id}>
									<div className="row">
										<div className="col-9">
											<span className="badge badge-secondary float-left">{name} {surname}</span>
											<span className="badge badge-info">{email}</span>
										</div>
										<div className="col-3 btn-group">
											<button className="btn btn-outline-primary btn-sm"
															onClick={(e) => this.handleEditButtonClick(id)}> Edit
											</button>
											<button className="btn btn-outline-dark btn-sm"
															onClick={(e) => this.handleDeleteButtonClick(id)}> Delete
											</button>
										</div>
									</div>
								</div>
							);
						})
					}
				</div>
				<div className="col-2">
					<button
						className="btn btn-success btn-sm mt-4"
						onClick={this.handleAddUserClick}
					>
						Add user
					</button>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		users: state.users
	};
};

export default connect(mapStateToProps, { addUser, updateUser, deleteUser, getUsers })(withRouter(UserList));