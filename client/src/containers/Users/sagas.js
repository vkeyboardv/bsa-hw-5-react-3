import axios from "axios";
import { SERVER_URL } from "../../config";
import { call, put, takeEvery, all } from "redux-saga/effects";
import { ADD_USER, UPDATE_USER, DELETE_USER, GET_USERS } from "./actionTypes";
import { getUsers as actionGetUsers, getUsersSuccess } from "./actionCreators";

export function* getUsers() {
	try {
		const response = yield call(axios.get, `${SERVER_URL}/users`);
		const data = yield response.data;
		yield put(getUsersSuccess(data));
	} catch (err) {
		console.log("Getting users has failed. Please check", err.message);
	}
}

export function* addUser(action) {
	const newUser = { ...action.payload.data, id: action.payload.id };
	try {
		yield call(axios.post, `${SERVER_URL}/users`, newUser);
		yield put(actionGetUsers());
	} catch (err) {
		console.log("Adding user has failed. Please check", err.message);
	}
}

export function* updateUser(action) {
	const id = action.payload.id;
	const updatedUser = { ...action.payload.data };

	try {
		yield call(axios.put, `${SERVER_URL}/users/${id}`, updatedUser);
		yield put(actionGetUsers());
	} catch (err) {
		console.log("Updating user has failed. Please check", err.message);
	}
}

export function* deleteUser(action) {
	const id = action.payload;
	try {
		yield call(axios.delete, `${SERVER_URL}/users/${id}`);
		yield put(actionGetUsers());
	} catch (err) {
		console.log("Deleting user has failed. Please check", err.message);
	}
}

export default function* usersSaga() {
	yield all([
		takeEvery(GET_USERS, getUsers),
		takeEvery(ADD_USER, addUser),
		takeEvery(UPDATE_USER, updateUser),
		takeEvery(DELETE_USER, deleteUser)
	]);
};
