import axios from "axios";
import { call, put, takeEvery, all } from "redux-saga/effects";
import { GET_USER } from "./actionTypes";
import { SERVER_URL } from "../../config";
import { getUserSuccess } from "./actionCreators";

export function* getUser(action) {
	try {
		const response = yield call(axios.get, `${SERVER_URL}/users/${action.payload}`);
		const data = yield response.data;
		yield put(getUserSuccess(data));
	} catch (err) {
		console.log("Getting the user failed. Please check", err.message);
	}
}

export default function* userSaga() {
	yield all([
		takeEvery(GET_USER, getUser)
	]);
};
