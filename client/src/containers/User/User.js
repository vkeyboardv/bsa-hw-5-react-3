import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getUser } from "./actionCreators";
import { addUser, updateUser } from "../Users/actionCreators";

const initialState = {
	"id": "",
	"name": "",
	"surname": "",
	"email": "",
	"password": ""
};

class User extends Component {

	state = initialState;

	componentDidMount() {
		const { match, getUser } = this.props;
		if (parseInt(match.params.id) > 0) {
			getUser(match.params.id);
		}
	}

	static getDerivedStateFromProps(props, state) {
		if (props.match.params.id && props.data.id !== state.id) {
			return {
				...props.data
			};
		} else {
			return null;
		}
	}

	handleCancelButtonClick = () => {
		this.setState(initialState);
		this.props.history.push("/users");
	};

	handleSaveButtonClick = () => {
		if (this.state.id) {
			this.props.updateUser(this.state.id, this.state);
		} else {
			this.props.addUser(this.state);
		}
		this.setState(initialState);
		this.props.history.push("/users");
	};

	handleInputChange = (event) => {
		const { name, value } = event.target;
		this.setState({ [name]: value });
	};

	render() {
		const { name, surname, email, password } = this.state;

		return (
			<div className="user-modal modal" style={{ display: "block" }}>
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ padding: "5px" }}>
						<div className="modal-header">
							<h5 className="modal-title">Add user</h5>
							<button type="button" className="close" data-dismiss="modal" aria-label="Close"
											onClick={this.handleCancelButtonClick}>
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<div className="form-group row">
								<label className="col-sm-3 col-form-label">Name</label>
								<input
									className="col-sm-9"
									value={name}
									name='name'
									type='text'
									onChange={e => this.handleInputChange(e)}
								/>
							</div>

							<div className="form-group row">
								<label className="col-sm-3 col-form-label">Surname</label>
								<input
									className="col-sm-9"
									value={surname}
									name='surname'
									type='text'
									onChange={e => this.handleInputChange(e)}
								/>
							</div>

							<div className='email-input form-group row'>
								<label className="col-sm-3 col-form-label">Email </label>
								<input
									value={email}
									type='email'
									name='email'
									onChange={e => this.handleInputChange(e)}
								/>
							</div>

							<div className="form-group row">
								<label className="col-sm-3 col-form-label">Password</label>
								<input
									className="col-sm-7"
									value={password}
									name='password'
									type='password'
									onChange={e => this.handleInputChange(e)}
								/>
							</div>

						</div>
						<div className="modal-footer">
							<button className="btn btn-secondary" onClick={this.handleCancelButtonClick}>Cancel</button>
							<button className="btn btn-primary" onClick={this.handleSaveButtonClick}>Save</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		data: state.user.data
	};
};

const mapDispatchToProps = {
	getUser,
	addUser,
	updateUser
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(User));