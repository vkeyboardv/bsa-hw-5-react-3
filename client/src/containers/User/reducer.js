import { GET_USER_SUCCESS } from "./actionTypes";

const initialState = {
	data: {
		name: "",
		surname: "",
		email: "",
		password: ""
	}
};

export default function (state = initialState, action) {
	switch (action.type) {
		case GET_USER_SUCCESS: {
			return {
				...state,
				data: action.payload
			};
		}

		default:
			return state;
	}
}
