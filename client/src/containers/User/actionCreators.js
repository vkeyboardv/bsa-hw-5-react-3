import { GET_USER, GET_USER_SUCCESS } from "./actionTypes";

export const getUser = (id) => {
	return {
		type: GET_USER,
		payload: id
	};
};

export const getUserSuccess = (data) => {
	return {
		type: GET_USER_SUCCESS,
		payload: data
	};
};