import React from "react";
import { connect } from "react-redux";
import { submitLoginForm } from "./actionCreators";

class Login extends React.Component {
	state = {
		login: "",
		password: ""
	};

	handleInputChange = (event) => {
		event.preventDefault();
		this.setState({
			[event.target.name]: event.target.value
		});
	};

	handleFormSubmit = (event) => {
		event.preventDefault();
		const { login, password } = this.state;
		this.props.submitLoginForm({ login, password });
	};

	render() {
		return (
			<div className="container">
				<div className="login-form-box d-flex justify-content-center align-items-center">
					<form onSubmit={e => this.handleFormSubmit(e)}>
						<div className="input-group mb-2">
							<input
								type="text"
								name="login"
								className="form-control"
								placeholder="Login"
								onChange={e => this.handleInputChange(e)}
							/>
						</div>
						<div className="input-group mb-2">
							<input
								type="password"
								name="password"
								className="form-control"
								placeholder="Password"
								onChange={e => this.handleInputChange(e)}
							/>
						</div>
						{(this.props.auth.currentUser === null) ? (<div className="login-error">Authentication error</div>) : null}

						<div className="col text-center">
							<button type="submit" className="btn btn-primary btn-md">Log In</button>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		auth: state.auth
	};
};

export default connect(mapStateToProps, { submitLoginForm })(Login);