import axios from "axios";
import { SERVER_URL } from "../../config";
import { call, put, takeEvery, all } from "redux-saga/effects";
import { SUBMIT_LOGIN_FORM } from "./actionTypes";
import { loginUser } from "./actionCreators";

function* getUserData(action) {
	try {
		const response = yield call(axios.post, `${SERVER_URL}/login`, action.payload);
		const data = yield response.data;
		yield put(loginUser(data));
	} catch (err) {
		yield put(loginUser(null));
		console.log("Authentication failed. Please check.", err.message);
	}
}

export default function* initLoginSagas() {
	yield all([
		takeEvery(SUBMIT_LOGIN_FORM, getUserData)
	]);
}