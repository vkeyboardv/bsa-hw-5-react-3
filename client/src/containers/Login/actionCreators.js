import {
	SUBMIT_LOGIN_FORM,
	LOGIN_USER
} from "./actionTypes";

export const submitLoginForm = (data) => {
	return {
		type: SUBMIT_LOGIN_FORM,
		payload: data
	};
};

export const loginUser = (data) => {
	return {
		type: LOGIN_USER,
		payload: data
	};
};