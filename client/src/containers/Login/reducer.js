import {
	LOGIN_USER
} from "./actionTypes";

const initialState = {
	currentUser: undefined,
	isLoggedIn: false,
	isAdmin: false
};

export default function (state = initialState, action) {
	switch (action.type) {
		case LOGIN_USER:
			return {
				...state,
				currentUser: action.payload,
				isLoggedIn: !!action.payload,
				isAdmin: !!(action.payload && action.payload.name === "admin")
			};
		default:
			return state;
	}
}