import axios from "axios";
import { SERVER_URL } from "../../config";
import { call, put, takeEvery, all } from "redux-saga/effects";
import { GET_MESSAGES, ADD_MSG, UPDATE_MSG, DELETE_MSG } from "./actionTypes";
import { getMessages as actionGetMessages, getMessagesSuccess, switchIsLoading } from "./actionCreators";

function* getMessages() {
	try {
		yield put(switchIsLoading(true));
		const response = yield call(axios.get, `${SERVER_URL}/messages`);
		const messages = yield response.data;
		yield put(getMessagesSuccess(messages));
		yield put(switchIsLoading(false));
	} catch (err) {
		console.log("Fetching messages failed. Please check", err.message);
	}
}

function* addMsg(action) {
	try {
		yield call(axios.post, `${SERVER_URL}/messages`, action.message);
		yield put(actionGetMessages());
	} catch (err) {
		console.log("Creating the message is failed. Please check", err.message);
	}
}


function* updateMsg(action) {
	try {
		yield call(axios.put, `${SERVER_URL}/messages/${action.payload.id}`, action.payload.message);
		yield put(actionGetMessages());
	} catch (err) {
		console.log("Updating the message is failed. Please check", err.message);
	}
}


function* deleteMsg(action) {
	try {
		yield call(axios.delete, `${SERVER_URL}/messages/${action.payload}`);
		yield put(actionGetMessages());
	} catch (err) {
		console.log("Deleting the message is failed. Please check", err.message);
	}
}

export default function* chatSaga() {
	yield all([
		takeEvery(GET_MESSAGES, getMessages),
		takeEvery(ADD_MSG, addMsg),
		takeEvery(UPDATE_MSG, updateMsg),
		takeEvery(DELETE_MSG, deleteMsg)
	]);
}