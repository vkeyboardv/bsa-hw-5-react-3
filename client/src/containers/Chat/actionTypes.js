export const GET_MESSAGES_SUCCESS = "GET_MESSAGES_SUCCESS";
export const GET_MESSAGES = "GET_MESSAGES";
export const SWITCH_IS_LOADING = "SWITCH_IS_LOADING";
export const ADD_MSG = "ADD_MSG ";
export const UPDATE_MSG = "UPDATE_MSG";
export const DELETE_MSG = "DELETE_MSG";