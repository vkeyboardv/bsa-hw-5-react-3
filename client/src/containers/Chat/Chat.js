import React from "react";
import Header from "../../components/Header/Header";
import MessageList from "../../components/MessageList/MessageList";
import MessageInput from "../../components/MessageInput/MessageInput";
import SpinnerElement from "../../components/SpinnerElement";

import { connect } from "react-redux";
import { getMessages, addMsg, deleteMsg, updateMsg } from "./actionCreators";

class Chat extends React.Component {
	componentDidMount() {
		this.props.getMessages();
	}

	render() {
		const { messages, isLoading, addMsg, deleteMsg, updateMsg } = this.props;
		if (!isLoading) {
			return (
				<div className="main-chat">
					<Header messages={messages} isLoading={isLoading} />
					<MessageList messages={messages} deleteMsg={deleteMsg}
											 updateMsg={updateMsg} />
					<MessageInput messages={messages} addMsg={addMsg} />
				</div>
			);
		} else {
			return (
				<div className="spinner">
					<SpinnerElement />
				</div>
			);
		}
	}
}

const mapStateToProps = state => {
	return {
		messages: state.chat.messages,
		isLoading: state.chat.isLoading
	};
};

export default connect(mapStateToProps, { getMessages, addMsg, deleteMsg, updateMsg })(Chat);
