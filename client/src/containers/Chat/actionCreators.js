import {
	SWITCH_IS_LOADING,
	GET_MESSAGES,
	ADD_MSG,
	UPDATE_MSG,
	DELETE_MSG,
	GET_MESSAGES_SUCCESS
} from "./actionTypes";

export const getMessages = () => {
	return {
		type: GET_MESSAGES
	};
};

export const getMessagesSuccess = (messages) => {
	return {
		type: GET_MESSAGES_SUCCESS,
		payload: messages
	};
};

export const switchIsLoading = (isLoading) => {
	return {
		type: SWITCH_IS_LOADING,
		payload: isLoading
	};
};

export const addMsg = (message) => {
	return {
		type: ADD_MSG,
		message
	};
};

export const deleteMsg = (id) => {
	return {
		type: DELETE_MSG,
		payload: id
	};
};

export const updateMsg = (message) => ({
	type: UPDATE_MSG,
	payload: { id: message.id, message }
});
