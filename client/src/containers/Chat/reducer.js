import {
	GET_MESSAGES_SUCCESS,
	SWITCH_IS_LOADING
} from "./actionTypes";

const initialState = {
	messages: [],
	isLoading: true
};

export default function (state = initialState, action) {
	switch (action.type) {
		case SWITCH_IS_LOADING:
			return {
				...state,
				isLoading: action.payload
			};
		case GET_MESSAGES_SUCCESS: {
			return {
				...state,
				messages: action.payload.reverse()
			};
		}
		default:
			return state;
	}
}