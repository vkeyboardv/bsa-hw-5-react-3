import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import store from "./getStore";
import "./App.css";
import Main from "./components/Main/Main";

class App extends React.Component {
	render() {
		return (
			<div className="App">
				<div className="App-wrapper">
					<Router>
						<Provider store={store}>
							<Main />
						</Provider>
					</Router>
				</div>
			</div>
		);
	}
}

export default App;
