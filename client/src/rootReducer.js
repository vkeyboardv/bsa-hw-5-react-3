import { combineReducers } from "redux";
import loginReducer from "./containers/Login/reducer";
import chatReducer from "./containers/Chat/reducer";
import userReducer from "./containers/User/reducer";
import usersReducer from "./containers/Users/reducer";

export default combineReducers({
	chat: chatReducer,
	auth: loginReducer,
	users: usersReducer,
	user: userReducer
});