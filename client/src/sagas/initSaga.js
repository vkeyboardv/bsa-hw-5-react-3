import { all, call } from "redux-saga/effects";
import chatSaga from "../containers/Chat/sagas";
import initLoginSagas from "../containers/Login/sagas";
import userSaga from "../containers/User/sagas";
import usersSaga from "../containers/Users/sagas";

const initializeSagas = function* initSaga() {
	yield all([
		call(chatSaga),
		call(initLoginSagas),
		call(userSaga),
		call(usersSaga)
	]);
};

export default initializeSagas;